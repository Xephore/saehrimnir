# saehrimnir

## About

Saehrimnir is a, locally stored, cookbook and nutrition managment app.

## Legal

All the nutrient data is sourced from the [Canadian Nutrient File, Health Canada, 2015](https://www.canada.ca/en/health-canada/services/food-nutrition/healthy-eating/nutrient-data/canadian-nutrient-file-2015-download-files.html). The Saehrimnir application is not an offical version of the marterials. The Saehrimnir application had no part in the creation of the [Canadian Nutrient File, Health Canada, 2015](https://www.canada.ca/en/health-canada/services/food-nutrition/healthy-eating/nutrient-data/canadian-nutrient-file-2015-download-files.html). The Seahrimnir application is in no way in affiliation with, nor has the endorsement of, Health Canada.

Both Saehrimnir and the [Canadian Nutrient File, Helath Canada, 2015](https://www.canada.ca/en/health-canada/services/food-nutrition/healthy-eating/nutrient-data/canadian-nutrient-file-2015-download-files.html) are intended as reference tools only, and are not medical advice.

